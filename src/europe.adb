with Ada.Text_IO; use Ada.Text_IO;

package body Europe is

   procedure Faire_Espagne is

      --        type Situation is (Maritime, Terrestre);
      --        type Emplacement is record
      --           logitude : Float;
      --           latitude : Float;
      --           endroit : Situation;
      --           -- en + : nom de la ville la + proche
      --           ville : string(1..15);
      --        end record;
      --        el_madrid: Emplacement:= (-1.0, 37.2, "Terrestre", "Madrid         ");--Il faudra mettre les espaces du string


      type Emplacement is record
         logitude : Float;
         latitude : Float;
         Maritime : string(1..8);
         Terrestre : string(1..9);
         -- en + : nom de la ville la + proche
         ville : string(1..6);
      end record;

      el_madrid: Emplacement:= (-1.0, 37.2, "Maritime", "Terrestre", "Madrid");
   begin
      Put_line("Espagne:");
      Put_line("Madrid : "& el_madrid.logitude'Image &
                 " " & el_madrid.latitude'Image &
                 " " & el_madrid.Terrestre &
                 " " & el_madrid.ville);

   end Faire_Espagne;

   procedure Faire_Italie is

      --le type d'orgatisation est fait par un package

      package Destination is
         -- destination : nom, pays, jours
         type Instance is tagged record -- Tagged solo se puede utilizar dentro tipo de paquete especifico.
            -- Una grabacion del paquete que el necesita y buscara para hacer los diferentes llamados
            nom : string(1..10);
            pays : string(1..2);
            jours : positive;
         end record;
         --Create une instance et la reenvoyer
         function Create ( nom: string := "          ";
                           pays : string := "??";
                           jours : positive :=1) return Instance;
         procedure Afficher(self : in out Instance);
         procedure Allonger(self : in out Instance ; ajout : integer);
         procedure Racourcir(self : in out Instance ; enleve : integer);
         function Heures (self : Instance) return positive;
         procedure AfficherAvecEtoiles(self : in out instance'class ); --Si quiero un affichage explicito, tengo que poner " 'class "
      end Destination;

      package body Destination is
         function Create ( nom: string := "          ";
                           pays : string := "??";
                           jours : positive :=1) return Instance is
            i: instance :=(nom, pays, jours);
         begin
            return i;
         end Create;

         procedure Afficher(self : in out Instance) is
         begin
            Put_Line(self.nom & self.pays & self.jours'Image);
         end Afficher;

         procedure Allonger (self : in out Instance ; ajout : integer) is
         begin
            self.jours := self.jours + ajout;
         end Allonger;

         procedure Racourcir (self : in out Instance ; enleve : integer) is
         begin
            --self.jours := self.jours - enleve;
            self.Allonger(-enleve);
         end Racourcir;

         function Heures(self : Instance) return positive is
         begin
            return 24 * self.jours;
         end;
         procedure AfficherAvecEtoiles(self : in out instance'class ) is
         begin
            Put_line("********");
            self.Afficher;
            Put_line("********");
         end AfficherAvecEtoiles;
      end Destination;

      ---------Creation of a new CONSTRUCTEUR for ILE --------
      package Destination_Maritime is
         type Instance is new Destination.Instance with record --NEW es un TYPE DERIVE
            ile : string(1..10);
         end record;
         overriding function Create ( nom: string := "          ";
                                      pays : string := "??";
                                      jours : positive :=1) return Instance;
         function Create ( nom: string := "          ";
                           pays : string := "??";
                           jours : positive :=1;
                           ile : string := "          ") return Instance;
         overriding procedure Afficher(self: in out Instance);

      end Destination_Maritime;

      package body Destination_Maritime is
         overriding function Create ( nom: string := "          ";
                                      pays : string := "??";
                                      jours : positive :=1) return Instance is
            i: Instance :=(nom, pays, jours, "          ");
         begin
            return i;
         end Create;

         function Create ( nom: string := "          ";
                           pays : string := "??";
                           jours : positive :=1;
                           ile : string := "          ") return Instance is
            i: Instance :=(nom, pays, jours, ile);
         begin
            return i;
         end Create;

         overriding procedure Afficher(self: in out Instance) is
         begin
            Put_line("**** Affichage 1 ****");
            Put_Line(self.nom & self.pays & self.jours'Image);
            Put_line("**** Affichage 2 ****");
            Destination.afficher(Destination.Instance(self));
            Put_line("**** Affichage 3 ****");
            Destination.Instance(self).afficher;
            Put_line("**** Affichage 4 ****");
            Put_line(" . " & self.ile);
         end Afficher;
      end Destination_Maritime;


      el_dest_0 : destination.Instance := destination.Create("Milan     ", "IT", 4);
      el_dest_1 : Destination_Maritime.Instance := Destination_Maritime.Create("Palermo   ","IT", 3, "Sicile    ");
      el_dest_2 : destination.Instance := el_dest_0;


   begin
      Put_Line("Italie :");
      el_dest_0.Afficher;
      el_dest_0.Allonger(2);
      el_dest_0.Racourcir(3);
      el_dest_0.Afficher;
      Put_line("En heures : " & el_dest_0.Heures'Image);
      el_dest_1.Afficher; --affichage hija
      Put_Line(" -------- ");
      el_dest_0.AfficherAvecEtoiles; -- affichage madre : Palermo
      el_dest_1.AfficherAvecEtoiles;-- Si pongo 'class va a llamar la madre y despues a la hija
      --el_dest_2.AfficherAvecEtoiles;-- Sin 'class va a llamar la madre

      el_dest_0.allonger(10);
      el_dest_2.afficher;
   end;


end europe;
