with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; --Entrer des entiers dans la console

package body amerique is

   type tjours is range 0 ..100;
   function Get_jours return tjours is
      jours_i : integer;
   begin
      Ada.Text_IO.Put_line("Jours?");
      Ada.Integer_Text_IO.Get(jours_i);
      return tjours(jours_i);
   end Get_Jours;

procedure faire_canada(avion : tavion := 1000.0 ; hotel:tprix := 100.0) is
   -- voyages au Canada : -20% sur 860e d'avion et 7j � 48e/j
   type tdiscount is digits 3 range 0.0 .. 100.0;
   discount : tdiscount := 20.0;
   --type tjours is range 0 .. 100;
   jours : tjours := 7;
   prix : tprix := 48.0;

   ------------2. Manipulation of types
   total : tprix;

   ------------3. Constants-------------
   --type tplanete is (mercure, venus, terre, mars);
   --p1 : tplanete := venus;
   --b : boolean := true;
   type tdays is (lundi, mardi, mecredi, jeudi, vendredi,samedi, dimanche);
   type tweekend is (samedi, dimanche);
   depart : tdays := samedi;

   ------------4. String-------------
   infos : string := "Deuxieme message = Voyage au Canada : -"& discount'Image &
     "% sur" & avion'Image & "e d'avion et" & jours'Image &
     "jours a" & prix'Image & "e/j";
   infostotal : string (1 .. 28);

   ------------5. Modulo-------------
   a : integer := -(7 mod (-4)); --R/ 1
   b : integer := -7 rem (-4); --R/ -3
   --c : integer := 5 or 6;      --
   voyage_moyen : boolean := jours > 3 and then jours < 10;

   -------------6. Syntaxe : type deriv�--------------
   type tjsemaine is new tdays range lundi .. vendredi;
   subtype tgrandweekend is tdays range vendredi .. dimanche;
   arrivee : tgrandweekend := dimanche;
   voldenuit : boolean := depart /= arrivee;

   -------------7. Subtype : Typedef------------
   subtype tgdweekend is tgrandweekend;

   -------------8. GET -------------
   --duree_i : integer;

   -------------9. Tableau--------
   type ttablindex is range 0 .. 2;
   type ttabl is array(ttablindex) of character;

   type ttab2 is array(0..2) of character;

   tabl : ttabl := ('A', 'B', 'C');
   --2eme type de tableau
   type ttkm is array(tjours range <>) of integer;
   --tkm1 : ttkm (1..100); --en comentaire pour eviter le warning

   type t2dims is array(1..4,2..3) of float; --tableau 4x2=8

   -------9.1 exercise
   type ttablekm is array(tjours) of integer;
   tkm : ttablekm;
   distance_km_avion : integer := 0;
   distance_km_repos : integer:= 0;
   distance_km_marche : integer:= 0;
   ttotalkm : integer:= 0;


begin
   Ada.Text_IO.Put_line("Bienvenue dans l'agence de voyage");
   jours := Get_jours;
   --jours := tjours(duree_i);
   Ada.Text_IO.Put_line("Voyage au Canada : -"& discount'Image &
                          "% sur" & avion'Image & "e d'avion et" & jours'Image &
                          "jours a" & prix'Image & "e/j");

   --2. Corriger : calculer le total complet
   total := tprix(float(prix) * float(jours) + float(avion));
   total := total * tprix(1.0 - float(discount) / 100.0);
   Ada.Text_IO.Put_line("Total prix Canada : " & total'Image & "e");

   --3. Constants
   Ada.Text_IO.Put_line("Depart le : " & depart'Image & ".");

   --3.1 Validation
   Ada.Text_IO.Put_line("Depart le : " & boolean(depart=dimanche)'Image & ".");

   --4. String: chaine de characteres
   -- c1 : character := 'A';
   -- s1 : string := "OK !";
   Ada.Text_IO.Put_line(infos);
   infostotal := "Total Canada : " & total'Image & "e";
   Ada.Text_IO.Put_line(infostotal);

   --5. Modulo
   -- 5 : 00000101
   -- 6 : 00000110
   --or : 00000111 : 7
   --xor: 00000011 : 3
   --and: 00000100 : 4
   Ada.Text_IO.Put_line(a'Image);
   Ada.Text_IO.Put_line(b'Image);
   --Ada.Text_IO.Put_line(c'Image);
   Ada.Text_IO.Put_line(voyage_moyen'Image);

   --6. Syntaxe
   Ada.Text_IO.Put_line(voldenuit'Image);

   -- 8.1 EXERCISE
   -- En general : Air Canada
   -- Voyages court (<7) : Quebec'Air
   -- Voyage de 7, 14, 21, 28j : Can'jet
   if jours < 7 then
      Ada.Text_IO.Put_Line("Quebec'Air");
   elsif jours = 7 or jours = 14 or jours = 21 or jours = 28 then
      Ada.Text_IO.Put_Line("Can'Jet");
   else
      Ada.Text_IO.Put_Line("Air Canada");
   end if;

   --autre option
   case jours is
      when 0 .. 6 => Ada.Text_IO.Put_Line("Quebec'Air");
      when 7|14|21|28 => Ada.Text_IO.Put_Line("Can'Jet");
      when others => Ada.Text_IO.Put_Line("Air Canada");
   end case;
   -- Disponible en ADA 2012
   Ada.Text_IO.Put_line(if jours < 45 or jours >50 then "Visa !" else "");

   --8.2 EXERCISE
   -- Jour 1 : avion
   -- Jour 2 : marche
   -- Jour 3 : marche
   -- Jour 4 : marche
   -- Jour 5 : repos
   -- Jour 6 : marche
   -- Jour 7 : avion

   Ada.Text_IO.Put_Line("Jour 1 : avion");
   for i in 2 .. jours loop
      if (i = 5 or i = 9) and (i /= jours) then
         Ada.Text_IO.Put_Line("Jour" & i'Image  &" : repos");
      elsif i<jours then
         Ada.Text_IO.Put("Jour" & i'Image & " : ");
         Ada.Text_IO.Put("marche");
         Ada.Text_IO.Put_line("");
      else
         Ada.Text_IO.Put_Line("Jour" & i'Image & " : avion");
      end if;
   end loop;

   Ada.Text_IO.Put_Line("");

   --9. Tableau

   tabl(ttabl'first) := '4';

   tkm(1..2) := (0, 0); --On cheche toujours un intervale de tableau


   --9.1 Exercise
   ----------Autre Solution-------------
   for j in 1 .. jours loop
      if j = 1 or j = jours then
         Ada.Text_IO.Put_line("Jour" & j'Image &" : avion");
         distance_km_avion := distance_km_avion + 6500;
      elsif j mod 4 = 1 then
         Ada.Text_IO.Put_line("Jour" & j'Image &" : repos");
         distance_km_repos := distance_km_repos + 0;
      else
         Ada.Text_IO.Put_line("Jour" & j'Image &" : marche");
         distance_km_marche := distance_km_marche + 22;
      end if;

      --Stoquer dans un tableau le nombre de km parcouru
      -- avion : 6500km
      -- marche : 22km

   end loop;

   --refaire une bouble pour compter le total de km, et afficher
   declare
      ttotalkm : integer;
   begin
      tkm(1) := distance_km_avion;
      tkm(2) := distance_km_repos;
      tkm(3) := distance_km_marche;

      ttotalkm := tkm(1) + tkm(2) + tkm(3);
      Ada.Text_IO.Put_line("Total" & ttotalkm'Image);
   end;
   end faire_Canada;



   end Amerique;
