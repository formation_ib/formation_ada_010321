with Ada.Text_IO;
procedure Main is

-- pragma Optimize(Off);
   a : integer := 3;
   b : integer := integer'first; -- 'first entier le plus petit possible
                                 -- 'last entier les plus grand posible
   c : integer := integer'last; -- 'first entier le plus petit possible
                                -- 'last entier les plus grand posible

   --une autre syntaxe
   d : integer := integer'first;
   --type tb is range 0 .. 1_000_000;
   --Version moduler
   type tb is mod 1_000_001;
   e : tb;

   f : float;
   type td is digits 10;
   g : td := 12.345;


begin
   Ada.text_IO.Put_line("Bienvenue dans l'agence de voyage");
   a := a + 5;
   Ada.Text_IO.Put_line(a'Image);
   Ada.text_IO.Put_line(b'Image);
   Ada.text_IO.Put_line(c'Image);

   -- d := tb'last - tb'first + 1;
   -- WARNING
   e := 999999;
   e := e + 2;
   Ada.Text_IO.Put_line("Fin 1");

   -- Avec
   e := 999999;
   e := e + 2;
   e := e - 2;
   Ada.Text_IO.Put_line("b =" & e'Image);
   Ada.Text_IO.Put_line("Fin 2");

   --
   Ada.Text_IO.Put_line("g =" & g'Image);
   Ada.Text_IO.Put_line("Fin 3");

end Main;
