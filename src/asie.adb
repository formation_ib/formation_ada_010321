with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded; --On l'utilise pour obtenir un sessi de l'utilisateur
with Ada.Strings.Fixed;
with Ada.Text_IO.Unbounded_IO; --On est pas oblig� a indiquer la taille du string
with Ada.Calendar; use Ada.Calendar;
with Ada.Calendar.Formatting;
with Ada.Containers.Vectors;


package body Asie is
   procedure Faire_Russie is
      dest, tel : string(1..10) := "          ";
      dest_1, tel_1 : natural;
      nom : Unbounded_String; --on recoit un suel information, une suele chaine
      maintenant : Ada.Calendar.Time := Ada.Calendar.Clock;
      preparation : duration;
      depart : Ada.Calendar.Time;
      f : File_Type; --Ada.Text_IO

   begin
      Put_line("Quelle ville voulez-vous voyager ?");
      Get_line(dest,dest_1);
      Put_line("Quel est votre nom ?");
      Ada.Text_IO.Unbounded_IO.Get_line(nom);
      Put_line("Quel est votre numeo telephone ?");
      Get_line(tel,tel_1);

      --Corriger nom et dest : "  Moscou  " => "Moscou"
      nom := Ada.Strings.Unbounded.Trim(nom, Ada.Strings.both);
      dest_1 := Ada.Strings.Fixed.Trim(dest, Ada.Strings.both)'Length;
      dest(1..dest_1) := Ada.Strings.Fixed.Trim(dest, Ada.Strings.both);

      Put_line("Voyage pour " & dest(1..dest_1) & " enregistre " &
                 "pour " & To_String(nom) & ". ");

      --Use of Calendar
      Put_line("Annee en cours : " & Ada.Calendar.Year(maintenant)'Image);
      Put_line("Maintenant: " & Ada.Calendar.Formatting.Image(maintenant));
      -- maintenant + d => time
      -- maintenant - d => time
      -- d + d => duration
      -- d - d => duration
      -- maintenant - maintenant => duration
      -- maintenant + maintenant => impossible

      --Afficher : depart au mieux le ../.. (4 semaines et 3j + tard)
      preparation := duration((7*4+3)*24*60*60);
      depart := maintenant + preparation;
      Put_line("Depart au mieux le " & Ada.Calendar.Day(depart)'Image & "/" &
                 Ada.Calendar.Month(depart)'Image);

      -- fabrique un fichier russie.csv, chaque ligne ressemble �:
      -- Moscou, Bob, 0965343425
      begin
         open(f, Append_File, "russie.csv");
      exception
         when name_error => Create(f, Out_File, "russie.csv");
      end;
      Put_line(f, dest & "," & To_String(nom) & "," & tel);
      Close(f);

   end Faire_Russie;

   procedure Faire_Japon is
      a1 : integer;
      type tage is range 0..120;
      age,min_age,max_age : tage;

      package tVectorAge is new Ada.Containers.Vectors(Index_Type=>Natural,
                                                   Element_Type=>tage);
      v1 : tVectorAge.vector;
   begin
      --Valide depuis ADA 2005
      --MAPS = dictionaire
      Put_line("Japon :");
      -- demander l'age des voyageurs, et stoquer dans un vecteur
      -- quand on rentre un 0, on sort et on relit le vecteur pour
      -- indiquer a la fin le + jeune et le plus vieux
      loop
         Put_line("Entrez age :");
         Get(a1);
         age := tage(a1);
         Put_line("Age :" & age'Image);
         exit when age = 0 ;
         v1.append(age);
      end loop;
      min_age := 120;
      max_age := 0;
      for i in 0 .. natural(v1.Length)-1 loop
         if min_age > v1(i) then
            min_age := v1(i);
         end if;
         if max_age < v1(i) then
            max_age := v1(i);
         end if;
      end loop;

      Put_line("Intervale des ages : " & min_age'Image & " -" & max_age'Image);

   End Faire_Japon;

end Asie;
