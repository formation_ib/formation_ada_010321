with Ada.Text_IO;
procedure Main is
   -- voyages au Canada : -20% sur 860e d'avion et 7j � 48e/j
   type tdiscount is digits 3 range 0.0 .. 100.0;
   discount : tdiscount := 20.0;
   type tavion is digits 6 range 0.0 .. 10000.0;
   avion : tavion := 860.0;
   type tjours is range 0 .. 100;
   jours : tjours := 7;
   type tprix is digits 6 range 0.0 .. 10000.0;
   prix : tprix := 48.0;

begin
   Ada.text_IO.Put_line("Bienvenue dans l'agence de voyage");
   Ada.text_IO.Put_line("Voyage au Canada : -"& discount'Image &
                          "% sur" & avion'Image & "e d'avion et" & jours'Image &
                          "jours a" & prix'Image & "e/j");

   --conversion interdite : total := prix * jours

end Main;
