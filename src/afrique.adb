with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Exceptions;

package body Afrique is

   function Get_Prix_Namibie (j:positive) return positive
   with pre => j>1 and j<50
   is
   begin
      return j*37*1210;
   end Get_Prix_Namibie;


   procedure Faire_Namibie is
      j:positive;
      ex_que7:Exception;
   begin
      Put_line("Namibie , jours ?");
      Get(j);
      if j mod 7 /= 0 then
         Ada.Exceptions.Raise_Exception(ex_que7'Identity, "Avion que le samedi");
         --ou
         raise ex_que7 with "Avion que le samedi";
      end if;
      Put_Line(j'image & "Jours enregistre");
      Put_Line("Prix TCC : " & Get_Prix_Namibie(j)'Image & "e");
   exception
      when e:ex_que7 => Put_Line("Exception . " &
                                Ada.Exceptions.Exception_Message(e));
      when e:constraint_error => Put_Line("Exception CE - " &
                                Ada.Exceptions.Exception_Message(e));
      when e:others => Put_Line("Exception others - " &
                                Ada.Exceptions.Exception_Message(e));
   end Faire_Namibie;
end Afrique;
